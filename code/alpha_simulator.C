
#include "alpha_simulator.h"

void alpha_simulator(int N_events, double dl, TString output, TString eloss = "./alpha_loss_in_experiment_for_simulation_NEW.txt", TString kinetic = "./Ne20_a_a_elastic.txt")
{ 
 TFile *out = new TFile(output, "recreate");
 TTree *tree = new TTree("home_sim", "home_sim");
 
 double E;  // keV
 double elost; // keV
 
 double F[3]; // point of the first step in active volume
 double L[3]; // point of the last step in active volume
 
 double R;  // R = (L-F)^2, mm
 
 double charge; // sum of the electrons that triggered pads
 
 double xv; // mm
 
 double Theta, theta, phi; // degrees
 
 int npads;
 
 int is_front, is_begin;
 
 tree->Branch("E",  &E,  "E/D");
 tree->Branch("dE", &elost, "dE/D");
 
 tree->Branch("F", F, "F[3]/D");
 tree->Branch("L", L, "L[3]/D");
 
 tree->Branch("R", &R, "R/D");
 
 tree->Branch("charge", &charge, "charge/D");
 
 tree->Branch("xv", &xv, "xv/D");
 
 tree->Branch("Theta", &Theta, "Theta/D");
 tree->Branch("theta", &theta, "theta/D"); 
 tree->Branch("phi",   &phi,   "phi/D");
 
 double B_orig[3];
 
 tree->Branch("B_orig", B_orig, "B_orig[3]/D");
 
 #ifdef use_reconstruction
	 double xv_rec;
	 double theta_rec, phi_rec;
	 double E_rec, R_rec;
	 double anchor[3], dir[3], stop[3];
	 double chi2;
	 
	 tree->Branch("xv_rec", &xv_rec, "xv_rec/D");
	 
	 tree->Branch("theta_rec", &theta_rec, "theta_rec/D"); 
	 tree->Branch("phi_rec",   &phi_rec,   "phi_rec/D");
	 
	 tree->Branch("E_rec", &E_rec, "E_rec/D");
	 tree->Branch("R_rec", &R_rec, "R_rec/D");
	 	 
	 tree->Branch("A_rec", anchor, "A_rec[3]/D");
	 tree->Branch("B_rec", dir,    "B_rec[3]/D");
	 tree->Branch("stop",  stop,   "stop[3]/D");
	 
	 tree->Branch("chi2", &chi2, "chi2/D");
	 
	 #ifdef is_beam
	     double theta_cm[2];
	     tree->Branch("theta_cm", theta_cm, "theta_cm[2]/D");
	 
	     double Ex;
	     tree->Branch("Ex", &Ex, "Ex/D");
      #endif
 #endif
 

 
 #ifdef save_event
 	 TH3F *the_event = new TH3F("the_event", "the_event", ncols, 0., side_x, nrows, 0., side_y, Nz, 0., active_height); 
 	 tree->Branch("the_event", the_event);
 	 
 	 TGraph *profile = new TGraph();
 	 profile->SetMarkerStyle(20);
 	 profile->SetMarkerSize(1.5);
 	 tree->Branch("profile", profile);
 	 
 	 TGraphErrors *bragg = new TGraphErrors();
 	 tree->Branch("bragg", bragg);
 	 
 	 double params[2];
 	 tree->Branch("params", params, "params[2]/D");
 	 
 #endif
  
 tree->Branch("npads", &npads, "npads/I");
  
 tree->Branch("is_begin", &is_begin, "is_begin/I");
 tree->Branch("is_front", &is_front, "is_front/I");
 
 int side, row, col;
 
 tree->Branch("side", &side, "side/I");
 
 tree->Branch("col", &col, "col/I");
 tree->Branch("row", &row, "row/I");
 
 TGraph *energy_loss_profile = new TGraph(eloss.Data(), "%lf %lf");
 
 energy_loss_profile->SetName("eloss_profile");
 energy_loss_profile->SetTitle("Energy loss profile");
 
 energy_loss_profile->GetXaxis()->SetTitle("Energy (keV)");
 energy_loss_profile->GetYaxis()->SetTitle("#frac{dE}{dx} (keV/mm)");
 
 energy_loss_profile->SetMarkerStyle(20);
 energy_loss_profile->SetMarkerColor(kRed);
 energy_loss_profile->SetLineWidth(3);
 energy_loss_profile->SetLineColor(kRed);
 
 TSpline3 *S = new TSpline3("S", energy_loss_profile); // energy loss profile: dE/dx (keV/mm) vs. E (keV)
 
 /*****/
 
 TGraph *xE = new TGraph();
  
 int n = energy_loss_profile->GetN();
 double *e = energy_loss_profile->GetX();
 double *s = energy_loss_profile->GetY();
 
 TGraph *inv_sE = new TGraph();
 
 for(int k=0; k<n; k++)
 	inv_sE->SetPoint(k, e[k], 1./(s[k]));
   
 for(int k=0; k<n-1; k++)
 { 
  double x = IntegrateProfile(inv_sE, e[0], e[k]);
  xE->SetPoint(k, x, e[k]);
 }
 
 /*****/
 
 #ifdef is_beam
    TGraph *kinetic_curve = new TGraph(kinetic.Data(), "%lf %lf"); 
    kinetic_curve->SetPoint(kinetic_curve->GetN(), 90., 0.);
    
    kinetic_curve->SetName("kin_curve");
    kinetic_curve->SetTitle("Kinetic Curve");
 
    kinetic_curve->GetXaxis()->SetTitle("#theta_{#alpha} (#circ)");
    kinetic_curve->GetYaxis()->SetTitle("Energy (MeV)");
 
    kinetic_curve->SetMarkerStyle(20);
    kinetic_curve->SetMarkerColor(kBlue);
    kinetic_curve->SetLineWidth(3);
    kinetic_curve->SetLineColor(kBlue);
    
    TSpline3 *K = new TSpline3("K", kinetic_curve); // kinematic curve: E (MeV) vs. theta (degrees)v
 #endif
 
 
 int pad_plane[ncols][nrows];
 
 TStopwatch sw;
 
 for(int n=0; n<N_events;)
 {   
  // line in 3D: X = A + B * t 
  TRandom3 ang_rand(0);
  TRandom3 rand_num(0);
  
  TVector3 A;
  TVector3 B;
  
  //---------------------------------------------------------------
  //------------- GENERATION OF THE ALPHA PARTICLE - BEGIN --------
  //---------------------------------------------------------------
  
  side = 0;
  phi = rand_num.Uniform(0., 2.*Pi());
  
  #ifdef is_beam
  
    double val, x;  
    
    do
    {
     Theta = ang_rand.Uniform(Theta_min*DegToRad(),Theta_max*DegToRad());
   
     val = 1./(Power(Sin(Theta/2.), 4));
     x = (max_x_section - min_x_section)*(ang_rand.Uniform(0,1e8)/1e8) + min_x_section;
    }
    while(not(x<val and Sin(Theta)<m_He/m_beam));

    theta = 0.5*(Pi() - ASin((m_beam/m_He)*Sin(Theta)) - Theta);
  
    xv = rand_num.Uniform(0., 128.); // the reaction vertex	
   
    A.SetXYZ(xv, side_y/2., height);  
    B.SetXYZ(Cos(theta), Sin(theta)*Cos(phi), Sin(theta)*Sin(phi));
    
    theta *= RadToDeg();  
    phi *= RadToDeg();  
    
    E = K->Eval(theta)*1000.; // keV
    
  #else
    theta = ACos(rand_num.Uniform(0.,1.));
    
    #ifdef is_test
    	xv = 64.;//rand_num.Uniform(0., 128.);
    	
    	A.SetXYZ(xv, side_y/2., source_z);  
    	theta = 88.*DegToRad();
    	phi = 250.*DegToRad();
	B.SetXYZ(Cos(theta), Sin(theta)*Cos(phi), Sin(theta)*Sin(phi));
	
	E = 900.; //rand_num.Uniform(0.,1.) + 500.; // keV	
    #else
    	
    	xv = source_x;
    	
	A.SetXYZ(source_x, source_y, source_z);  
	B.SetXYZ(-Cos(theta), -Sin(theta)*Cos(phi), Sin(theta)*Sin(phi));
	
	double choice = rand_num.Uniform(0.,1.);
   
   	if(choice<1./3)
    		E = 5150.; // keV
    	
	if(choice>=1./3 and choice<2./3)
    		E = 5450.;
    	
	if(choice>=2./3)
    		E = 5805.;
    		
    #endif
    
    theta *= RadToDeg();  
    phi *= RadToDeg();
  #endif   
  
  //---------------------------------------------------------------
  //------------- GENERATION OF THE ALPHA PARTICLE - END ----------
  //---------------------------------------------------------------
  
  for(int k=0; k<3; k++)
  	B_orig[k] = B[k];
  
  #ifdef use_reconstruction
  	TH3F actual_event("actual_event", "actual_event", ncols, 0., side_x, nrows, 0., side_y, Nz, 0., active_height);
  	actual_event.Reset();
  	
  	chi2 = -1;
  	
  	E_rec = -1;
  	R_rec = -1;
  #endif
    
  elost = 0;
  R = 0;
  npads = 0;
  
  for(int i=0; i<3; i++)
  {
   F[i] = -1000;
   L[i] = -1000;
  }
  
  if(n%_STEP_ == 0)
  	cout<<"Event: "<<n<<endl;
  if(n == N_events - 1)
  	cout<<"Event: "<<N_events<<endl;
  	 
  reset_hit_pads(pad_plane);
  
  double energy = E;
  double l = 0;
  
  bool is_first_step = false;
  bool is_last_step  = false;
  
  is_begin = -1;
  is_front = -1;
  
  charge = 0;
    
  #ifdef save_event
  	the_event->Reset();
  	profile->Set(0);
  	bragg->Set(0);
  #endif
  
  double y_oscar;
  #ifdef is_beam
  	if(B[1]>0)
  	{
  	 side = 1;
  	 y_oscar = d_OSCAR_UP;
  	}
  	else
  	{
  	 side = -1;
  	 y_oscar = d_OSCAR_DO;
  	} 	
  #else
     #ifdef is_test
  	if(B[1]>0)
  	{
  	 side = 1;
  	 y_oscar = d_OSCAR_UP;
  	}
  	else
  	{
  	 side = -1;
  	 y_oscar = d_OSCAR_DO;
  	}
     #else
     	if(B[1]<0)
  	{
  	 side = 1;
  	 y_oscar = d_OSCAR_DO;
  	}
  	else
  	{
  	 side = -1;
  	 y_oscar = d_OSCAR_UP;
  	}
     #endif
  #endif
    
  col = -1;
  row = -1;
  
  // -----------------------------------------------------
  // ---------------- START ALPHA TRACKING ---------------
  // -----------------------------------------------------
  
  while(energy>0.)
  {  
   double dE = S->Eval(energy)*dl;
   
   dE += rand_num.Gaus(0,sigma_B*Sqrt(dl));
   
   if(dE>energy)
   	dE = energy;

   energy -= dE;
   l += dl;
    
   TVector3 X_el = A + B*l;
   
    /* 
   if(Abs((X_el[1] - y_oscar))<2.)
   {
    double t_hit = (y_oscar - A[1])/B[1];
  
    double x_hit = A[0] + B[0]*t_hit;
    double z_hit = A[2] + B[2]*t_hit;
    
    for(int c=0; c<oscar_cols; c++)
    {
     for(int r=0; r<oscar_rows; r++)
     {
      bool cond = (r==0 and c==0) or (r==0 and c==oscar_cols-1) or (r==oscar_rows-1 and c==0) or (r==oscar_rows-1 and c==oscar_cols-1);
     
      if(cond)
     	continue;
     	
      double pd_center_x = (2*c+1)*half_lateral_frame + c*pd_side + pd_side/2; 
      double pd_center_z = bottom_distance + (r+1)*lower_frame + r*upper_frame + r*pd_side + pd_side/2; 
    
      if(Abs(x_hit - pd_center_x)<=pd_side/2 and Abs(z_hit - pd_center_z)<=pd_side/2)
      {
       col = c+1;
       row = r+1;
       break;
      }
     }
    }
    
    energy = 0.;
   } 
   */

   if(not(is_inside_active_volume(X_el)))
   	continue;
   	
   // -----------------------------------------------------
   // ---- FROM NOW ON I AM INSIDE THE ACTIVE VOLUME ------
   // -----------------------------------------------------
   
   elost += dE;  
 
   if(not(is_first_step))
   {
    is_first_step = true;
     
    for(int j=0; j<3; j++)
    	F[j] = X_el[j];
     
    #ifdef is_beam
     	if(X_el[0]>0 and X_el[0]<4 and X_el[1]>0 and X_el[1]<64 and X_el[2]>0 and X_el[2]<170)
     		is_begin = 1;
    #else
     	if(X_el[0]>124 and X_el[0]<128 and X_el[1]>0 and X_el[1]<64 and X_el[2]>0 and X_el[2]<170)
     		is_begin = 1;
    #endif 
   }// end if is first step
    
   TVector3 X_el_post = A + B*(l+dl);
   double dE_post = S->Eval(energy)*dl;
     
   if(not(is_inside_active_volume(X_el_post)) or energy-dE_post<0)
   {
    is_last_step = true;
     
    for(int j=0; j<3; j++)
     	L[j] = X_el[j];
     
    #ifdef is_beam
     	if(X_el[0]>124 and X_el[0]<128 and X_el[1]>0 and X_el[1]<64 and X_el[2]>0 and X_el[2]<170)
     		is_front = 1;
    #else
     	if(X_el[0]>0 and X_el[0]<4 and X_el[1]>0 and X_el[1]<64 and X_el[2]>0 and X_el[2]<170)
     		is_front = 1;
    #endif 
   }// end if is last step  
 	
   int N_el = (int)dE/W_e;
   
   #ifdef save_event
    	profile->SetPoint(profile->GetN(), (X_el-A).Mag(), dE/dl);
   #endif
   
   for(int j=0; j<N_el; j++)
   {
    pair<double, double> X_PP = apply_diffusion(X_el);	
    
    double x = X_PP.first;
    double y = X_PP.second;
       
    if(x>0 and x<side_x and y>0 and y<side_y)
    {
     set_hit_pad(x, y, pad_plane);
     #ifdef use_reconstruction
     	actual_event.Fill(x, y, X_el.Z() + rand_num.Uniform(-dz, dz));
     #endif
     
     #ifdef save_event
     	the_event->Fill(x,y,X_el.Z() + rand_num.Uniform(-dz, dz));
     #endif
    }
   } 
  }
  
  // -----------------------------------------------------
  // ------------------ END ALPHA TRACKING ---------------
  // -----------------------------------------------------
  
  if(is_first_step and is_last_step)
  {  
   for(int k=0; k<3; k++)
   {
    double d = L[k] - F[k];
    R += d*d;
   }
   
   R = Sqrt(R); 
        
   npads = count_hit_pads(pad_plane, charge);
   
   #ifdef use_reconstruction
	vector<Point3d> cloud = Point3d::convert_histogram(&actual_event, my_offset);
   
	TVector3 A_rec = calculate_anchor(cloud);
	   
	Line3d line_rec;
	   
	#ifdef is_beam
	   	TVector3 B_guess(0.5,0.5,0.5);
	#else
	 	#ifdef is_test 
	   		TVector3 B_guess(0.5,0.5,0.5);
	   	#else
	   		TVector3 B_guess(-0.5, 0.5, 0.5);
	   	#endif	
	#endif
	
	   line_rec.fit_inliers(cloud, A_rec, B_guess);

	   TVector3 B_rec = line_rec.get_direction();
	   
          #ifdef is_beam
	    if(B_rec[0]<0)
	   	B_rec = -1*B_rec;
	#else
	#ifdef is_test
	    if(B_rec[0]<0)
	   	B_rec = -1*B_rec;
	#else
	    if(B_rec[0]>0)
	   	B_rec = -1*B_rec;
	#endif
	#endif
	   	
   
      double t_rec = (side_y/2. - A_rec[1])/B_rec[1];
    
      xv_rec = A_rec[0] + B_rec[0]*t_rec;
  
      theta_rec = ACos(Abs(B_rec[0]))*RadToDeg();
      phi_rec = ACos(B_rec[1]/Sqrt(B_rec[1]*B_rec[1]+B_rec[2]*B_rec[2]))*RadToDeg();
		    
      for(int k=0; k<3; k++)
      {
       dir[k] = B_rec[k];
       anchor[k] = A_rec[k];
      }
   
      #ifdef is_beam
   	if(B_rec[2]<0)
   	   	    	phi_rec = 360. - phi_rec;   	
      #else
   	#ifdef is_test
   	    if(B_rec[2]<0)
   	   	    	phi_rec = 360. - phi_rec;
   	#else
   	   	    if(B_rec[2]>0)
   	   	    	phi_rec = 180. - phi_rec;
   	   	    else
   	   	    	phi_rec = 180. + phi_rec;
   		#endif
   	 #endif
    
  #ifdef is_beam
  
  Point3d V_rec(xv_rec, side_y/2., height, 0.);
  
  #ifdef save_event
  	  R_rec = calculate_range_from_Bragg(cloud, V_rec, A_rec, B_rec, bragg, params);
  #else
  	  R_rec = calculate_range_from_Bragg(cloud, V_rec, A_rec, B_rec);
  #endif
  
  stop[0] = xv_rec    + B_rec[0]*R_rec;
  stop[1] = side_y/2. + B_rec[1]*R_rec;
  stop[2] = height    + B_rec[2]*R_rec;
    
  E_rec = calculate_energy_from_range(xE, R_rec);
  
  Ex = calculate_excitation_energy(E_rec, B_rec, theta_cm);
  
  #else
  
  #ifdef is_test
  
  Point3d V_rec(xv, source_y, source_z, 0.);
  
    #ifdef save_event
  	  R_rec = calculate_range_from_Bragg(cloud, V_rec, A_rec, B_rec, bragg, params);
    #else
  	  R_rec = calculate_range_from_Bragg(cloud, V_rec, A_rec, B_rec);
    #endif
    
    stop[0] = V_rec.get_x() + B_rec[0]*R_rec;
    stop[1] = V_rec.get_y() + B_rec[1]*R_rec;
    stop[2] = V_rec.get_z() + B_rec[2]*R_rec;
  
  #else
  double  X_top[3];  
  double  X_bottom[3];
  
  double  X_left[3];
  double  X_right[3];
  
  double  X_front[3];
  
  double  X_IN[3];
  double  X_PT[3];
     
  for(int j=0; j<3; j++)
  {   
   X_IN[j] = -1;
   X_PT[j] = -1;
  }
  
  X_top[0] = source_x + B[0]/B[2]*(active_height - source_z);
  X_top[1] = source_y + B[1]/B[2]*(active_height - source_z);
  X_top[2] = active_height;
  
  X_bottom[0] = source_x + B[0]/B[2]*( - source_z);
  X_bottom[1] = source_y + B[1]/B[2]*( - source_z);
  X_bottom[2] = 0.;
  
  X_left[0] = source_x + B[0]/B[1]*( - source_y);
  X_left[1] = 0.;
  X_left[2] = source_z + B[2]/B[1]*( - source_y);
  
  X_right[0] = source_x + B[0]/B[1]*(side_y - source_y);
  X_right[1] = side_y;
  X_right[2] = source_z + B[2]/B[1]*(side_y - source_y);  
  
  X_front[0] = 0.;
  X_front[1] = source_y + B[1]/B[0]*( - source_x);
  X_front[2] = source_z + B[2]/B[0]*( - source_x);
    
  
  if((X_top[0]>0 and X_top[0]<128) and (X_top[1]>0 and X_top[1]<64))
  {
   X_PT[0] = X_top[0];
   X_PT[1] = X_top[1];
   X_PT[2] = X_top[2];
  }
  else if((X_bottom[0]>0 and X_bottom[0]<128) and (X_bottom[1]>0 and X_bottom[1]<64))
  {
   X_PT[0] = X_bottom[0];
   X_PT[1] = X_bottom[1];
   X_PT[2] = X_bottom[2];
  }
  else if((X_left[0]>0 and X_left[0]<128) and (X_left[2]>0 and X_left[2]<170))
  {
   X_PT[0] = X_left[0];
   X_PT[1] = X_left[1];
   X_PT[2] = X_left[2];
  }
  else if((X_right[0]>0 and X_right[0]<128) and (X_right[2]>0 and X_right[2]<170))
  {
   X_PT[0] = X_right[0];
   X_PT[1] = X_right[1];
   X_PT[2] = X_right[2];
  }
  else if((X_front[1]>0 and X_front[1]<64) and (X_front[2]>0 and X_front[2]<170))
  {
   X_PT[0] = X_front[0];
   X_PT[1] = X_front[1];
   X_PT[2] = X_front[2];
   
   is_front = 1;
  }
   
   X_IN[0] = side_x;
   X_IN[1] = source_y + B[1]/B[0]*(side_x - source_x);
   X_IN[2] = source_z + B[2]/B[0]*(side_x - source_x); 
   
   for(unsigned int k=0; k<3; k++)
	   R_rec += Power(X_IN[k] - X_PT[k], 2);
	
   R_rec = Sqrt(R_rec);
   
   
   #endif
   #endif  
  
   
   #endif
  }
   
  tree->Fill();
     
  n++;
 }
 // end loop on events

 
 double time_elapsed = sw.RealTime();
 
 TNamed *We = new TNamed("Electron/ion production cost (eV)", Form("%3.1f", 1000.*elect_th));
 TNamed *th = new TNamed("N electron threshold", Form("%d", elect_th));
 TNamed *step = new TNamed("Step (mm)", Form("%4.2f", dl));
 TNamed *time_per_event = new TNamed("Event per second ", Form("%4.2f", N_events/time_elapsed));
 
 tree->SetAlias("rose_sin", "theta*TMath::Sin(phi*TMath::DegToRad())");
 tree->SetAlias("rose_cos", "theta*TMath::Cos(phi*TMath::DegToRad())");
 
 tree->SetAlias("is_stopped_in_volume", "L[0]>8 && L[0]<120 && L[1]>10 && L[1]<54 && L[2]>10 && L[2]<160");

 #ifdef use_reconstruction
 	tree->SetAlias("rose_sin_rec", "theta_rec*TMath::Sin(phi_rec*TMath::DegToRad())");
 	tree->SetAlias("rose_cos_rec", "theta_rec*TMath::Cos(phi_rec*TMath::DegToRad())");
 	
 	tree->SetAlias("is_stopped_in_volume_rec", "stop[0]>8 && stop[0]<120 && stop[1]>10 && stop[1]<54 && stop[2]>10 && stop[2]<160");
 #endif

 tree->Write();
 
 th->Write();
 step->Write();
 time_per_event->Write();
 energy_loss_profile->Write();
 
 #ifdef is_beam
    kinetic_curve->Write();
 #endif
 
 out->Close();
 
}


/***********************************************************************************/
/***********************************************************************************/
/***********************************************************************************/
/***********************************************************************************/

int count_hit_pads(int pads[ncols][nrows], double &charge)
{
 int npads = 0;
 
 for(int i=0; i<ncols; i++)
 {
  for(int j=0; j<nrows; j++)
  {
   bool cond;
	   
   #ifdef is_beam
   	cond = (i>=7) and ((j>=5 and j<=12) or (j>=19 and j<=26));
   #else
   	cond = true;
   #endif
 
   if(not(cond))
   	continue;
   
   int n_el = pads[i][j];
 
   if(n_el>=elect_th)
   {
    npads++;
    charge += n_el;
   }
  }
 }
  
 return npads;
}

/******************************************************************************/

void set_hit_pad(double x, double y, int pads[ncols][nrows])
{
 int xp = (int)x/pad_size;
 int yp = (int)y/pad_size;
 
 pads[xp][yp]++;
}

/******************************************************************************/

void reset_hit_pads(int pads[ncols][nrows])
{
 for(int i=0; i<ncols; i++)
 {
  for(int j=0; j<nrows; j++)
  {
   pads[i][j]=0;
  }
 }
}

/******************************************************************************/

bool is_inside_active_volume(TVector3 pos)
{
 bool cond_x = pos.X()>0 and pos.X()<side_x;
 bool cond_y = pos.Y()>0 and pos.Y()<side_y;
 bool cond_z = pos.Z()>0 and pos.Z()<active_height;
 
 return (cond_x and cond_y and cond_z);
}

/******************************************************************************/

pair<double, double> apply_diffusion(TVector3 pos)
{
 TRandom3 rand_num(0);

 pair<double, double> X_PP;
 
 double x0 = pos.X();	
 double y0 = pos.Y();
 double z0 = pos.Z();
 
 double sigma = sqrt(2.*Dt*z0/vd);
 
 X_PP.first  = rand_num.Gaus(x0, sigma);
 X_PP.second = rand_num.Gaus(y0, sigma);
 
 return X_PP;
}

/****************************************************************/

TVector3 calculate_direction_with_chi2_random(vector<Point3d> cloud, TVector3 A, TVector3 B_guess, double &chi2)
{
 TRandom3 random_number(0);
 
 TVector3 opt_direction = B_guess;
 	
 double min_chi2 = 1e300;
 
 int n_tries = 2000;
  
 for(int k=0; k<n_tries;)
 {
  double mag2 = 0.; 
  double bx=0, by=0, bz=0; 
  
  do
  {
   //if(k==0)
   	//bx = random_number.Uniform(0, 1);  	
   //else
   	bx = random_number.Uniform(opt_direction[0]*0.5, opt_direction[0]*1.5);
   	by = random_number.Uniform(opt_direction[1]*0.5, opt_direction[1]*1.5);
   	bz = random_number.Uniform(opt_direction[2]*0.5, opt_direction[2]*1.5);
   
   //by = random_number.Uniform(-Sqrt(1-bx*bx), Sqrt(1-bx*bx));
   //bz = random_number.Uniform(-Sqrt(1-bx*bx), Sqrt(1-bx*bx));
   
   mag2 = bx*bx + by*by + bz*bz;
  }
  while(mag2<0.995 or mag2>1.005 or isnan(by) or isnan(bz));
  
  // calculate sum of distance from the line
  TVector3 tmp_B(bx,by,bz);
  
  Line3d tmp_line(A, tmp_B);
  
  double sum_distances = 0;
  double W = 0;
  for(Point3d P : cloud)
  {
   double w = Power(P.get_e(), 3);
   sum_distances += w*tmp_line.distance_from_point(P);
   W += w;
  }
  
  sum_distances /= W;
  
  if(sum_distances<min_chi2)
  {
   min_chi2 = sum_distances;
   opt_direction = tmp_B;
  }
  
  k++;
 }
  
 chi2 = min_chi2;

 return opt_direction;
}

/********************************************************************/

TVector3 calculate_direction_with_grid_chi2(vector<Point3d> cloud, TVector3 A, TVector3 B_guess, double &chi2)
{
 int Nrand = 100;
 
 TVector3 opt_direction(0,0,0);
 double min_chi2 = 1e300; 
 
 double bx, by, bz;
 
  
 for(int i=0; i<Nrand+1; i++)
  {
   //bx = 1./Nrand*i;
   bx = B_guess[0]/Nrand*i + 0.5*B_guess[0]; // among 0.5x and 1.5x B_guess[0]
   
   for(int j=0; j<Nrand+1; j++)
   {
    //by = 1./Nrand*j;
    
    if(B_guess[1]>0)
    	by =  B_guess[1]/Nrand*j + 0.5*B_guess[1];  // among 0.5x and 1.5x B_guess[1]
    else
    	by = -B_guess[1]/Nrand*j + 1.5*B_guess[1];  // among 0.5x and 1.5x B_guess[1]

    //if(B_guess[1]<0)
    //	by--;
    
    bz = Sqrt(1. - bx*bx - by*by);
    
    if(isnan(bz))
    	continue;
    
    if(B_guess[2]<0)
    	bz *=-1;
    
    TVector3 tmp_B(bx,by,bz);
  
    Line3d tmp_line(A, tmp_B);
  
    double sum_distances = 0;
    double W = 0;
     
    for(Point3d P : cloud)
    {
     double w = Power(P.get_e(), 3);
     sum_distances += w*tmp_line.distance_from_point(P);
     W += w;
    }
  
    sum_distances /= W;
  	
    if(sum_distances<min_chi2)
    {
     min_chi2 = sum_distances;
     opt_direction = tmp_B;
    }
   }
  }
  
 chi2 = min_chi2;
  
 return opt_direction; 
}

/********************************************************************/

TVector3 calculate_direction_with_projections(vector<Point3d> cloud)
{ 
 double sxx=0, sxy=0, sxz=0, sx=0, sy=0, sz=0, n=0;
 
 for(Point3d P : cloud)
 {
  array<double, 4> coords = P.get_xyze();
  
  double x = coords[0];
  double y = coords[1];
  double z = coords[2];
  double e = coords[3]*coords[3]*coords[3];
  
  //cout<<"x: "<<x<<" y: "<<y<<" z: "<<z<<" e: "<<e<<endl;
    
  sxx += e*x*x;
  
  sxy += e*x*y;
  sxz += e*x*z;
  
  sx += e*x;
  sy += e*y;
  sz += e*z;
  
  n += e;  
 }
 
 double D = sxx*n - sx*sx;
 
 double m_yx = (sxy*n-sx*sy)/D; 
 double m_zx = (sxz*n-sx*sz)/D;
 
 TVector3 direction;
 
 double bx = 1./Sqrt(1. + m_yx*m_yx + m_zx*m_zx);
 double by = m_yx * bx;
 double bz = m_zx * bx;

 direction.SetXYZ(bx,by,bz);
 

 //Point3d B(bx, by, bz, 0);	
 //cout<<"B (2D proj): "<<B<<endl;

 return direction;
}

/********************************************************************/

TVector3 calculate_anchor(vector<Point3d> cloud)
{
 TVector3 anchor(0,0,0);
 
 double W = 0;
  
 double ax=0, ay=0, az=0;
 
 for(Point3d P : cloud)
 {
  double w = Power(P.get_e(), 3);
  
  ax += P.get_x()*w;
  ay += P.get_y()*w;
  az += P.get_z()*w;
  
  W += w;
 }
 
 ax /= W;
 ay /= W;
 az /= W;
 
 anchor.SetXYZ(ax,ay,az);
 
 //Point3d A(ax, ay, az, 0);
 //cout<<"A: "<<A<<endl;
 
 return anchor;
}

/*********************************************/

double IntegrateProfile(TGraph *sp, double a, double b)
{
 TSpline3 profile("profile", sp);

 double integral=0;
 double s = (b-a)/nInt;
 
 double x = a;
 
 while(x<b)
 {
  integral += (s/6)*(profile.Eval(x) + profile.Eval(x+s) + 4*profile.Eval((2*x+s)/2));
  x+=s;
 }
 
 return integral;
}

/*********************************************/

double bragg_fitter(double *x, double *par)
{
 // 0 --> alfa
 // 1 --> mu
 // 2 --> sigma
 // 3 --> tau
 
 double f = par[0]*Exp(par[3]*par[3]*par[2]*par[2]/2+par[3]*(x[0]-par[1]))*(1-Erf((x[0]-par[1])/sqrt(2)/par[2]+par[3]*par[2]/sqrt(2)));
 
 return f; 
}

#ifdef save_event

double calculate_range_from_Bragg(vector<Point3d> cloud, Point3d vertex, TVector3 A_rec, TVector3 B_rec, TGraphErrors *bragg, double params[2])
{
 double range = 0;
  
 TH3F cloud_histo("cloud_histo", "cloud_histo", ncols, 0., side_x, nrows, 0., side_y, Nz, 0., active_height);
 
 for(Point3d P : cloud)
 {
  array<double, 4> p = P.get_xyze();
  //cout<<P<<endl;
  cloud_histo.Fill(p[0], p[1], p[2], p[3]);
 }
 
 
 vector<Point3d> CoG;
 
 if(abs(ATan(B_rec[1]/B_rec[0])*RadToDeg())<45.) // sum on x
 {
  for(int i=1; i<=ncols; i++)
  {
   int n = 0;
   Point3d C(i*pad_size,0,0,0);
   
   for(int k=1; k<=Nz; k++)
   {
    for(int j=1; j<=nrows; j++)
    {
     int idbin = cloud_histo.GetBin(i,j,k);
     double cbin = cloud_histo.GetBinContent(idbin); 
     
     if(cbin>0)
     {
     // cout<<i*pad_size<<"  "<<j*pad_size<<"  "<<k*dz<<"  "<<cbin<<endl;
      C.set_y(C.get_y() + cbin*j*pad_size);
      C.set_z(C.get_z() + cbin*k*dz);
     
      C.set_e(C.get_e() + cbin);
      
      n++;
     }
    }
   } 
   
   if(C.get_e()>0)
   {
    C.set_y(C.get_y()/C.get_e());
    C.set_z(C.get_z()/C.get_e());
    
    C.set_e(C.get_e()/n);
       
    CoG.push_back(C); 
   } 
  }
  
 }
 else
 {
 
  for(int j=1; j<=nrows; j++)
  {
   int n = 0;
   Point3d C(0,j*pad_size,0,0);
   
   for(int k=1; k<=Nz; k++)
   {
    for(int i=1; i<=ncols; i++)
    {
     int idbin = cloud_histo.GetBin(i,j,k);
     double cbin = cloud_histo.GetBinContent(idbin); 
     
     if(cbin>0)
     {
      C.set_x(C.get_x() + cbin*i*pad_size);
      C.set_z(C.get_z() + cbin*k*dz);
     
      C.set_e(C.get_e() + cbin);
      
      n++;
     }
    }
   } 
   
   if(C.get_e()>0)
   {
    C.set_x(C.get_x()/C.get_e());
    C.set_z(C.get_z()/C.get_e());
    
    C.set_e(C.get_e()/n);
    
    //C.set_e(C.get_e()/Cos(theta));
    
    //cout<<C<<endl;
   
    CoG.push_back(C); 
   }
  } 
  
 }

 vector<double> distances;
 map<double, double> dist_energy;
 map<double, double> dist_error;
 
 //cout<<"\nVertex: "<<vertex<<endl;
 
 for(Point3d P : CoG)
 {
  //cout<<P<<endl;
  double e = P.get_e();
  
  double t = B_rec.Dot(P.get_xyz()-A_rec)/B_rec.Mag2(); 
  TVector3 h = A_rec + B_rec*t;
  
  Point3d H(h,0);
  double d = vertex.distance(H); 
  
  //cout<<H<<"  "<<d<<endl;
  
  distances.push_back(d);
 
  dist_energy[d] = e;
  dist_error[d]  = Sqrt(e); 
 }
 
 int _SIZE_ = distances.size();
 if(_SIZE_<5)
 	return 0; 
      
 sort(distances.begin(), distances.end());
  
 vector<double> energies;
 vector<double> errors;
  
 for(int i=0; i<_SIZE_; i++)
 {
  double d = distances[i];
  energies.push_back(dist_energy[d]);
  errors.push_back(dist_error[d]);
  
 // cout<<d<<"  "<<dist_energy[d]<<"  "<<dist_error[d]<<endl;
 }
   
 double d_guess = 0;
 double peak = 0;
 
 for(int i=0; i<_SIZE_; i++)
 {
 
  if(energies[i]>peak and distances[i]>10.)
  {
   peak = energies[i];
   d_guess = distances[i];
  }
  
  bragg->SetPoint(i, distances[i], energies[i]);  
  bragg->SetPointError(i, 0., errors[i]);
 
 }
   
 TF1 *ff = new TF1("bragg_fitter", bragg_fitter, 0.5*d_guess, distances[_SIZE_-1], 4);
 
 ff->SetParameters(peak/1.67, d_guess, 0., 0.);
  
 //ff->SetParLimits(0, 0.7*peak, 1.2*peak); 
 ff->SetParLimits(1, d_guess*0.8,  distances[_SIZE_-1]);
 
 ff->FixParameter(2, 21.8);
 ff->FixParameter(3, 3.6e-3);
   
 bragg->Fit(ff, "RNQ");
 
 params[0] = ff->GetParameter(0);
 params[1] = ff->GetParameter(1);
  
  /*    
 double f_max = ff->GetMaximum();
 double p_max = ff->GetMaximumX();
 
 double T = 0.1*f_max; 
 
 double x0, x1;
 double f0, f1, f;
   
 x0 = p_max + pad_size;
 x1 = 180.;
 
 f0 = ff->Eval(x0) - T;
 f1 = ff->Eval(x1) - T;
  
 do
 {
  range = (x0 + x1)/2.;
  f = ff->Eval(range) - T;
 
  //cout<<range<<"  "<<x0<<"  "<<x1<<endl;
  if(f0*f<0)
 	x1 = range;
  else
 	x0 = range; 
 }
 while(abs(x1-x0)>0.001);

 //delete bragg;
  
 //cout<<"Range: "<<range<<endl;
 */
 //range = 1.1636*ff->GetParameter(2) + params[1];
 range = 1.3624*ff->GetParameter(2) + params[1];
 
 return range;
}

#else

double calculate_range_from_Bragg(vector<Point3d> cloud, Point3d vertex, TVector3 A_rec, TVector3 B_rec)
{ 
 vector<double> distances;
 map<double, double> dist_energy;
 map<double, double> dist_error;
 
 for(Point3d P : cloud)
 {
  double e = P.get_e();
  
  double t = B_rec.Dot(P.get_xyz()-A_rec)/B_rec.Mag2(); 
  TVector3 h = A_rec + B_rec*t;
  
  Point3d H(h,0);
  double d = vertex.distance(H); 
    
  distances.push_back(d);
 
  dist_energy[d] = e;
  dist_error[d]  = Sqrt(e); 
 }
 
 int _SIZE_ = distances.size();
 if(_SIZE_<5)
 	return 0; 
      
 sort(distances.begin(), distances.end());
  
 vector<double> energies;
 vector<double> errors;
  
 for(int i=0; i<_SIZE_; i++)
 {
  double d = distances[i];
  energies.push_back(dist_energy[d]);
  errors.push_back(dist_error[d]);
 }
  
 TH1F *bragg = new TH1F("bragg", "bragg", 100,0,200);
  
 double d_guess = 0;
 double peak = 0;
  
 for(int i=0; i<_SIZE_; i++)
 {
 
  if(energies[i]>peak and distances[i]>10.)
  {
   peak = energies[i];
   d_guess = distances[i];
  }
  //cout<<distances[i]<<"  "<<energies[i]<<endl;
  
  bragg->Fill(distances[i], energies[i]);
  /*
  bool near = false;
  

  double d, e, error;
  
  if((distances[i+1] - distances[i])<2.)
  {
   d = (distances[i] + distances[i+1])/2.;
   e = energies[i] + energies[i+1];
   error = Sqrt(errors[i]*errors[i] + errors[i+1]*errors[i+1]);
   near = true;
  }
  
  int n = bragg->GetN();
  
  if(near)
  {
   bragg->SetPoint(n, d, e);
   
   bragg->SetPointError(n, 0., error); 	
   
   i++;
  }
  else
  {
   bragg->SetPoint(n, distances[i], energies[i]);
   
   bragg->SetPointError(n, 0., errors[i]);
  }
  */
 } 
 
 peak = bragg->Integral();
 
 d_guess = bragg->GetBinCenter(bragg->FindLastBinAbove(0))/2.;
 
 TF1 *ff = new TF1("bragg_fitter",bragg_fitter, 0.5*d_guess, distances[_SIZE_-1], 4);
 
 ff->SetParameters(peak, d_guess, 0., 0.);
  
 //ff->SetParLimits(0, 0.1*peak, 1.2*peak); 
 ff->SetParLimits(1, 0., distances[_SIZE_-1]);
 
 ff->FixParameter(2, 19.5);
 ff->FixParameter(3, 4.06e-3);
   
 bragg->Fit(ff, "RMQN");
    
 peak = ff->GetMaximum();
  
 double p_max = ff->GetMaximumX();
 double T = 0.1*peak; 
  
 double x0, x1, range;
 double f0, f1, f;
   
 x0 = p_max + pad_size/2.;
 x1 = 200.;
 
 f0 = ff->Eval(x0) - T;
 f1 = ff->Eval(x1) - T;
  
 do
 {
  range = (x0 + x1)/2.;
  f = ff->Eval(range) - T;
 
  if(f0*f<0)
 	x1 = range;
  else
 	x0 = range; 
 }
 while(abs(x1-x0)>0.001);
 
 delete bragg;
 
 return range;
}

#endif

/***************************************************/

double calculate_energy_from_range(TGraph *xE, double R_rec)
{
 TSpline3 sp("sp", xE);
 
 return sp.Eval(R_rec);
}

/***************************************************/

double calculate_excitation_energy(double E, TVector3 B_rec, double theta_cm[2])
{
 double E_MeV = E/1000.;
     
 double v_alpha_lab_modulus = c_light*Sqrt(2*(E_MeV)/m_He);
     
 // velocity of the alpha particle in the LAB frame
 double v_alpha[3];
 
 for(int i=0; i<3; i++)
 	v_alpha[i] = v_alpha_lab_modulus*B_rec[i];
  
 // velocity before scattering in the LAB frame
 double v_beam[3];
 
 v_beam[0] = V_beam;
 v_beam[1] = 0.;
 v_beam[2] = 0.;
 
 // velocity after scattering in the LAB frame
 double v_beam_lab_modulus = 0;
 
 for(int i=0; i<3; i++)
 {
  v_beam[i] -= (m_He/m_beam)*v_alpha[i];
  v_beam_lab_modulus += v_beam[i]*v_beam[i];
 }
 
 v_beam_lab_modulus = Sqrt(v_beam_lab_modulus);
 v_beam_lab_modulus /= c_light;
 
 double v_beam_cm_modulus = Power(v_beam[0]-v_cm, 2) + Power(v_beam[1], 2) + Power(v_beam[2], 2);
 v_beam_cm_modulus = Sqrt(v_beam_cm_modulus);
 
 theta_cm[0] = RadToDeg()*ACos((v_beam[0]-v_cm)/v_beam_cm_modulus);	

 double v_alpha_cm_modulus = Power(v_alpha[0]-v_cm, 2) + Power(v_alpha[1], 2) + Power(v_alpha[2], 2);
 v_alpha_cm_modulus = Sqrt(v_alpha_cm_modulus);
 
 theta_cm[1] = RadToDeg()*ACos((v_alpha[0]-v_cm)/v_alpha_cm_modulus);
 
 double E_beam_scatt = 0.5*m_beam*v_beam_lab_modulus*v_beam_lab_modulus;

 return E_beam - E_MeV - E_beam_scatt;
}





