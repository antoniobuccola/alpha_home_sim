#ifndef alpha_simulator_h
#define alpha_simulator_h

// SIMULATION PARAMETERS

#define is_beam 1
	
#ifndef is_beam
	#define is_test 1
#endif

#define use_reconstruction

#define _STEP_ 100

/*
#ifdef use_reconstruction
	//#define use_chi2 1
#endif
*/
#define save_event

#include <iostream>
#include <math.h>
#include <utility>
#include <map>

using namespace std;

#include "TSystem.h"
#include "TCanvas.h"
#include "TStopwatch.h"
#include "TNamed.h"
#include "TString.h"
#include "TRandom3.h"
#include "TFile.h"
#include "TTree.h"
#include "TVector3.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TAxis.h"
#include "TSpline.h"
#include "TMath.h"
#include "TF1.h"

#include "Point3d.h"
#include "Line3d.h"

using namespace TMath;

const TVector3 my_offset(1,1,0);

#define c_light 300 // mm/ns
#define amu 931.5  // MeV/c^2
#define E_beam 997.17 // MeV  average among energy at the beginning and at the end of active volume
	
const double m_He   = amu*4.003;   // MeV/c^2
const double m_beam = amu*19.992;
	
const double V_beam = c_light*Sqrt(2*E_beam/m_beam); // ~ 98 mm/ns
const double v_cm   = (m_beam/(m_beam+m_He))*V_beam; // ~ 82 mm/ns

#define pad_size 2. // mm

#define ncols 64
const double side_x = pad_size*ncols;

#define nrows 32
const double side_y = pad_size*nrows;

#define active_height 170. // mm

#define vd 0.01297  // mm/ns   
#define Ts 80.      // ns

//#define Dl 9.326e-5 // mm^2/ns
#define Dt 1.132e-4 // mm^2/ns


const double dz = vd*Ts;
const int Nz = (int)active_height/dz;

#ifdef is_beam
	const double height = 95.;   // mm
#else
          const double source_x = 158.;
          const double source_y = side_y/2.;
          const double source_z = 95.;
#endif

#define Theta_max 2. // degrees, beam angle
const double min_x_section = 1./Power(Sin(Theta_max*DegToRad()/2.), 4);

#define Theta_min 0.3 // degrees, beam angle
const double max_x_section = 1./Power(Sin(Theta_min*DegToRad()/2.), 4);

/* 
	OSCARs parameters
*/
#define oscar_cols 12
#define oscar_rows 7

#define bottom_distance 50. // mm, distance floor of ATS - OSCARs 
#define lateral_gas_gap 65. // 

const double d_OSCAR_UP = side_y + lateral_gas_gap;  
const double d_OSCAR_DO = -lateral_gas_gap; 

#define half_lateral_frame 1.3 // mm
#define upper_frame 2.8
#define lower_frame 1.6
#define pd_side 10. 

// for scattered alphas
// trigger xp:  7 - 64 <--> x: 14 - 128

// trigger yp:  5 - 12 <--> y: 10 - 26
//             19 - 26 <--> y: 38 - 54	

// for source generated alpha, the trigger is the whole pad plane
#ifdef is_beam
	#define elect_th 410  // 410
#else
	#define elect_th 130  // 130
#endif

#define sigma_B 1.3  // 1.3 keV * sqrt(dl/mm)

#define W_e 0.03 // energy pad threshold = elect_th * W_e [keV]

#ifdef save_event
	double calculate_range_from_Bragg(vector<Point3d> cloud, Point3d vertex, TVector3 A_rec, TVector3 B_rec, TGraphErrors *bragg, double params[2]);
#else
	double calculate_range_from_Bragg(vector<Point3d> cloud, Point3d vertex, TVector3 A_rec, TVector3 B_rec);
#endif

double calculate_excitation_energy(double E, TVector3 B_rec, double theta_cm[2]);

double calculate_energy_from_range(TGraph *xE, double R_rec);

const int nInt = 2000;
double IntegrateProfile(TGraph *sp, double a, double b);

bool is_inside_active_volume(TVector3 pos);

void set_hit_pad(double x, double y, int pads[ncols][nrows]);

int count_hit_pads(int pads[ncols][nrows], double &charge);

void reset_hit_pads(int pads[ncols][nrows]);

pair<double, double> apply_diffusion(TVector3 pos);

TVector3 calculate_anchor(vector<Point3d> cloud);

TVector3 calculate_direction_with_chi2_random(vector<Point3d> cloud, TVector3 A, TVector3 B_guess, double &chi2);

TVector3 calculate_direction_with_grid_chi2(vector<Point3d> cloud, TVector3 A, TVector3 B_guess, double &chi2);

TVector3 calculate_direction_with_projections(vector<Point3d> cloud);


/* the real simulator */
void alpha_simulator(int N_events, double dl, TString output, TString eloss, TString kinetic);
#endif
